import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReservasListadoComponent } from './reservas-listado.component';
import { ReservasApiClientService } from '../reservas-api-client.service';



@NgModule({
  declarations: [ReservasListadoComponent],
  imports: [
    CommonModule
  ]
 
})
export class ReservasListadoModule {
  constructor(private api: ReservasApiClientService) { }
  
  ngOnInit() {
  }
 }
