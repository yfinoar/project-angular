export class DestinoViaje {
    nombre: String;
    imagenUrl: String;
    id: string;
    private selected: boolean;
    servicios: string[];
    
    constructor(nombre:String, url:String, public votes: number = 0){
        this.nombre = nombre;
        this.imagenUrl = url;
        this.servicios = ['Parqueadero', 'Desayuno'];
    
    }
    isSelected(): boolean {
        return this.selected;
    }
    setSelected(s: boolean) {
        this.selected = s;
    }
    voteUp(): any {
        this.votes++;
      }
      voteDown(): any {
        this.votes--;
      }
}