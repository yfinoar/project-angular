import { Component, EventEmitter, forwardRef, Inject, OnInit, Output } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl, ValidatorFn } from '@angular/forms';
import { fromEvent } from 'rxjs';
import { map, filter, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';
import { DestinoViaje } from '../../models/destino-viaje.model';
import { ajax, AjaxResponse } from 'rxjs/ajax';
import { AppConfig, APP_CONFIG } from 'src/app/app.module';
@Component({
  selector: 'app-form-destino-viaje',
  templateUrl: './form-destino-viaje.component.html',
  styleUrls: ['./form-destino-viaje.component.css']
})
export class FormDestinoViajeComponent implements OnInit {
@Output() onItemAdded: EventEmitter<DestinoViaje>;
fg: FormGroup;
minLongitud = 8;
searchResults: string[];

  constructor(fb: FormBuilder, @Inject(forwardRef(() => APP_CONFIG)) private config: AppConfig) { 
    // fb ayuda a crear el form group
    this.onItemAdded = new EventEmitter();
    this.fg = fb.group({
      // Form control que etsamos creando para vincular en el html
      nombre: ['', Validators.compose([
        Validators.required,
        this.nombreValidatorParametrizable(this.minLongitud)
      ])],
      url: ['url']
    });
    this.fg.valueChanges.subscribe((form: any) => {
      console.log("Cambió el formulario: ", form);
    });
  }

  ngOnInit() {
    let elementNombre = <HTMLInputElement>document.getElementById('nombre');
    fromEvent(elementNombre, 'input').pipe(
      map((e: KeyboardEvent) => (e.target as HTMLInputElement).value),
      filter(text => text.length > 4),
      debounceTime(200),
      distinctUntilChanged(),
      switchMap((text: string) => ajax(this.config.apiEndpoint + '/ciudades?q=' + text))
    ).subscribe(AjaxResponse => {
      this.searchResults = AjaxResponse.response;
    })
  }
  guardar(nombre: string, url: string): boolean {
    const d = new DestinoViaje(nombre, url);
    this.onItemAdded.emit(d);
    return false;
  }


nombreValidator(control: FormControl): {
  [s: string]: boolean } {
  const longitud = control.value.toString().trim().length;
  if(longitud > 0 && longitud < 3){
    return { invalidNombre: true };
  }
    return null;
  }

  nombreValidatorParametrizable(minLongitud: number): ValidatorFn {
    return (control: FormControl): { [s: string]:boolean} | null => {
      const longitud = control.value.toString().trim().length;
      if(longitud > 0 && longitud < minLongitud){
        return { minLongNombre: true };
      }
      return null;
    }
  }
}

